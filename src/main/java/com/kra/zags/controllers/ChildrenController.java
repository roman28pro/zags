package com.kra.zags.controllers;

import com.kra.zags.Main;
import com.kra.zags.domains.Child;
import com.kra.zags.domains.Marriage;
import com.kra.zags.domains.Person;
import com.kra.zags.domains.enums.MarriageStatus;
import com.kra.zags.domains.enums.Sex;
import com.kra.zags.services.ChildService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ChildrenController {
    private final ChildService childService;

    @FXML
    private ListView<Child> list;

    @Autowired
    public ChildrenController(ChildService childService) {
        this.childService = childService;
    }

    public void back(ActionEvent actionEvent) throws Exception {
        MainController.load();
    }

    public void update(ActionEvent actionEvent) {
        ObservableList<Child> personObservableList = FXCollections.observableArrayList();

        personObservableList.addAll(childService.getChildren());

        list.setCellFactory(param -> new ListCell<Child>() {
            @Override
            protected void updateItem(Child item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null) {
                    setText("");
                    return;
                }

                String text = "Номер: " + item.getChild().getId();
                text += " Фамилия: " + item.getChild().getLastName();
                text += " Имя: " + item.getChild().getFirstName();
                text += " Отчество: " + item.getChild().getSecondName();

                if (item.getParents().size() > 0) {
                    for (Person parent : item.getParents()) {
                        text += " Родитель: " + parent.getId();
                    }
                }

                setText(text);
            }
        });

        list.setItems(personObservableList);
    }

    public static void load() throws Exception {
        FXMLLoader loader = new FXMLLoader(MainController.class.getResource("/child_list.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }
}
