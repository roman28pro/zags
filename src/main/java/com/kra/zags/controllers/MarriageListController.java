package com.kra.zags.controllers;

import com.kra.zags.Main;
import com.kra.zags.domains.Marriage;
import com.kra.zags.domains.Person;
import com.kra.zags.domains.enums.MarriageStatus;
import com.kra.zags.domains.enums.PersonStatus;
import com.kra.zags.domains.enums.Sex;
import com.kra.zags.services.MarriageService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import org.springframework.stereotype.Controller;

@Controller
public class MarriageListController {
    private final MarriageService marriageService;

    @FXML
    private ListView<Marriage> list;

    public MarriageListController(MarriageService marriageService) {
        this.marriageService = marriageService;
    }

    public void back(ActionEvent actionEvent) throws Exception {
        MainController.load();
    }

    public void update(ActionEvent actionEvent) {
        ObservableList<Marriage> personObservableList = FXCollections.observableArrayList();

        personObservableList.addAll(marriageService.getMarriages());

        list.setCellFactory(param -> new ListCell<Marriage>() {
            @Override
            protected void updateItem(Marriage item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null) {
                    setText("");
                    return;
                }
                String text = "Номер: " + item.getId() +
                        " Дата регистрации: " + item.getRegistrationDate() +
                        " Описание: " + item.getDescription() +
                        " Статус:" + (item.getStatus() == MarriageStatus.IN_MARRIAGE ? "В браке" : "В разводе");

                if (item.getPeople().size() == 2) {
                    if (item.getPeople().get(0).getSex() == Sex.MALE) {
                        text += " Муж: " + item.getPeople().get(0).getId();
                        text += " Жена: " + item.getPeople().get(1).getId();
                    } else {
                        text += " Жена: " + item.getPeople().get(0).getId();
                        text += " Муж: " + item.getPeople().get(1).getId();
                    }
                }

                if (item.getDivorceReason() != null) {
                    text += " Причина развода: " + item.getDivorceReason();
                }

                if (item.getDivorceDate() != null) {
                    text += " Дата развода: " + item.getDivorceDate();
                }

                setText(text);
            }
        });

        list.setItems(personObservableList);
    }

    public static void load() throws Exception {
        FXMLLoader loader = new FXMLLoader(MainController.class.getResource("/marriage_list.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }
}
