package com.kra.zags.controllers;

import com.kra.zags.Main;
import com.kra.zags.domains.Person;
import com.kra.zags.domains.enums.PersonStatus;
import com.kra.zags.domains.enums.Sex;
import com.kra.zags.services.PersonService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

@Controller
public class PersonsListController {
    @FXML
    private ListView<Person> list;
    private PersonService personService;

    @Autowired
    public PersonsListController(PersonService personService) {
        this.personService = personService;

    }

    private void fillListView() {
        ObservableList<Person> personObservableList = FXCollections.observableArrayList();

        for (Person p : personService.getPersons()) {
            personObservableList.add(p);
        }
        list.setCellFactory(param -> new ListCell<Person>() {
            @Override
            protected void updateItem(Person item, boolean empty) {
                super.updateItem(item, empty);
                System.out.println(item);
                if (item == null) {
                    setText("");
                    return;
                }
                String text = "Номер: " + item.getId() +
                        " Фамилия: " + item.getLastName() +
                        " Имя: " + item.getFirstName() +
                        " Отчество:" + item.getSecondName() +
                        " Пол: " + (item.getSex() == Sex.MALE ? "М" : "Ж") +
                        " Дата рождения: " + item.getBirthDate() +
                        " Дата смерти: " + (item.getDeathDate() != null ? item.getDeathDate() : "-") +
                        " Статус: " + (item.getStatus() == PersonStatus.ALIVE ? "Жив" : "Умер");
                setText(text);
            }
        });

        list.setItems(personObservableList);
    }

    public static void load() throws Exception {
        FXMLLoader loader = new FXMLLoader(PersonRegistrationController.class.getResource("/persons_list.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }

    public void updateButtonOnClick(ActionEvent actionEvent) {
        fillListView();
    }

    public void backButtonOnClick(ActionEvent actionEvent) throws Exception {
        MainController.load();
    }
}
