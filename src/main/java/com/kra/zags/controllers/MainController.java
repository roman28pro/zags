package com.kra.zags.controllers;

import com.kra.zags.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.io.IOException;

@Controller
public class MainController {

    @FXML
    public void personsListButtonOnClick(ActionEvent actionEvent) {
        try {
            PersonsListController.load();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void load() throws Exception {
        FXMLLoader loader = new FXMLLoader(MainController.class.getResource("/main.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }

    @FXML
    public void onRegistrationButtonClick(ActionEvent actionEvent) {
        try {
            PersonRegistrationController.load();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void divorceRegistrationOnClick(ActionEvent actionEvent) throws IOException {
        DivorceRegistrationController.load();
    }

    @FXML
    public void deathRegistrationButtonOnClick(ActionEvent actionEvent) throws Exception {
        DeathRegistrationController.load();
    }

    @FXML
    public void marriageRegistrationButtonOnClick(ActionEvent actionEvent) throws IOException {
        MarriageRegistrationController.load();
    }

    @FXML
    public void marriageList(ActionEvent actionEvent) throws Exception {
        MarriageListController.load();
    }

    @FXML
    public void childrenList(ActionEvent actionEvent) throws Exception {
        ChildrenController.load();
    }
}
