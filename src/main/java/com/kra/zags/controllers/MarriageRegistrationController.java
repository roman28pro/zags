package com.kra.zags.controllers;

import com.kra.zags.Main;
import com.kra.zags.services.MarriageService;
import com.kra.zags.utils.PopupWindow;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class MarriageRegistrationController {
    private final MarriageService marriageService;

    @FXML
    private TextField description;

    @FXML
    private TextField person2;

    @FXML
    private DatePicker registrationDate;

    @FXML
    private TextField person1;


    @Autowired
    public MarriageRegistrationController(MarriageService marriageService) {
        this.marriageService = marriageService;
    }

    public void registrationButtonOnClick(ActionEvent actionEvent) throws ParseException {
        Long personId1 = null;
        Long personId2 = null;
        try {
            personId1 = Long.valueOf(person1.getText());
            personId2 = Long.valueOf(person2.getText());
        } catch (Exception e) {
            PopupWindow.openWindow("Ошибка", "Введите номер");
            return;
        }

        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(registrationDate.getValue().toString());
        String reason = description.getText();

        try {
            marriageService.addMarriage(reason, date, personId1, personId2);
            MainController.load();
        } catch (Exception e) {
            PopupWindow.openWindow("Ошибка", e.getMessage());
        }
    }

    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(PersonRegistrationController.class.getResource("/marriage_registration.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }

    public void back(ActionEvent actionEvent) throws Exception {
        MainController.load();
    }
}
