package com.kra.zags.controllers;

import com.kra.zags.Main;
import com.kra.zags.utils.PopupWindow;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.springframework.stereotype.Controller;

@Controller
public class DeathRegistrationController {

    @FXML
    private TextField personId;

    @FXML
    private TextArea description;

    @FXML
    private DatePicker deathDate;

    public static void load() throws Exception {
        FXMLLoader loader = new FXMLLoader(DeathRegistrationController.class.getResource("/death_registration.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }

    public void registrationButtonOnClick(ActionEvent actionEvent) {
        Long id = null;
        String reason = description.getText();

        try {
            id = Long.valueOf(personId.getText());
        } catch (Exception e) {
            PopupWindow.openWindow("Ошибка", "Введите номер");
        }
    }

    public void back(ActionEvent actionEvent) throws Exception {
        MainController.load();
    }
}
