package com.kra.zags.controllers;


import com.kra.zags.Main;
import com.kra.zags.domains.Person;
import com.kra.zags.domains.enums.Sex;
import com.kra.zags.services.PersonService;
import com.kra.zags.utils.PopupWindow;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class PersonRegistrationController {
    @FXML
    private TextField firstNameTextField;

    @FXML
    private TextField lastNameTextField;

    @FXML
    private TextField secondNameTextField;

    @FXML
    private TextField dadNumber;

    @FXML
    private DatePicker birthDateField;

    @FXML
    private TextField momNumber;

    @FXML
    private Button addButton;

    @FXML
    private RadioButton maleRadioButton;

    @FXML
    private RadioButton femaleRadioButton;

    private PersonService personService;

    @Autowired
    public PersonRegistrationController(PersonService personService) {
        this.personService = personService;
    }

    @FXML
    void addButtonOnClick(ActionEvent event) throws ParseException {
        Person person = new Person();
        person.setFirstName(firstNameTextField.getText());
        person.setSecondName(secondNameTextField.getText());
        person.setLastName(lastNameTextField.getText());
        person.setBirthDate(new SimpleDateFormat("yyyy-MM-dd").parse(birthDateField.getValue().toString()));

        Long momId = null;
        Long dadId = null;

        if (momNumber.getText() != null && !momNumber.getText().equals("")) {
            try {
                momId = Long.valueOf(momNumber.getText());
            } catch (Exception e) {
                PopupWindow.openWindow("Ошибка", "Поля не являются номером");
                return;
            }
        }

        if (dadNumber.getText() != null && !dadNumber.getText().equals("")) {
            try {
                dadId = Long.valueOf(dadNumber.getText());
            } catch (Exception e) {
                PopupWindow.openWindow("Ошибка", "Поля не являются номером");
                return;
            }
        }

        if (maleRadioButton.isSelected()) {
            person.setSex(Sex.MALE);
        }

        if (femaleRadioButton.isSelected()) {
            person.setSex(Sex.FEMALE);
        }

        try {
            personService.addPerson(person, dadId, momId);
            MainController.load();
        } catch (Exception e) {
            PopupWindow.openWindow("Ошибка", e.getMessage());
            return;
        }
    }

    public static void load() throws Exception {
        FXMLLoader loader = new FXMLLoader(PersonRegistrationController.class.getResource("/person_registration.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }

    public void back(ActionEvent actionEvent) throws Exception {
        MainController.load();
    }
}
