package com.kra.zags.controllers;

import com.kra.zags.Main;
import com.kra.zags.services.MarriageService;
import com.kra.zags.utils.PopupWindow;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class DivorceRegistrationController {
    private final MarriageService marriageService;

    @Autowired
    public DivorceRegistrationController(MarriageService marriageService) {
        this.marriageService = marriageService;
    }

    @FXML
    private TextField id;

    @FXML
    private TextField reason;

    @FXML
    private DatePicker date;

    @FXML
    void register(ActionEvent event) throws ParseException {
        Long marriageId = null;

        try {
            marriageId = Long.valueOf(id.getText());
        } catch (Exception e) {
            PopupWindow.openWindow("Ошибка", "Введите номер");
            return;
        }

        Date marriageDate =
                new SimpleDateFormat("yyyy-MM-dd").
                        parse(date.getValue().toString());

        try {
            marriageService.registerDivorce(marriageId, reason.getText(), marriageDate);
            MainController.load();
        } catch (Exception e) {
            PopupWindow.openWindow("Ошибка", e.getMessage());
        }
    }

    public static void load() throws IOException {
        FXMLLoader loader = new FXMLLoader(PersonRegistrationController.class.getResource("/divorce_registration.fxml"));
        loader.setControllerFactory(Main.getApplicationContext().getBeanFactory()::getBean);
        Parent view = loader.load();
        Main.getStage().setScene(new Scene(view));
        Main.getStage().show();
    }

    public void back(ActionEvent actionEvent) throws Exception {
        MainController.load();
    }
}
