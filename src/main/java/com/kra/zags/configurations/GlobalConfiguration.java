package com.kra.zags.configurations;

import com.kra.zags.services.PersonService;
import lombok.Data;

@Data
public class GlobalConfiguration {
    private static PersonService personService;
}
