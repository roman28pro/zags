package com.kra.zags.domains.enums;

public enum MarriageStatus {
    IN_MARRIAGE, IN_DIVORCE
}
