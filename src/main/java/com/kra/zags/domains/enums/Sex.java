package com.kra.zags.domains.enums;

public enum Sex {
    MALE, FEMALE
}
