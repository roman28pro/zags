package com.kra.zags.domains.enums;

public enum PersonStatus {
    ALIVE, DEAD
}
