package com.kra.zags.domains;

import com.kra.zags.domains.enums.MarriageStatus;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "marriages")
public class Marriage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;
    @Temporal(value = TemporalType.DATE)
    @Column(name = "registration_date")
    private Date registrationDate;
    @Column(name = "divorce_reason")
    private String divorceReason;
    @Temporal(value = TemporalType.DATE)
    @Column(name = "divorce_date")
    private Date divorceDate;
    @Temporal(value = TemporalType.DATE)
    @Column(name = "updated_at")
    private Date updatedAt;
    @Enumerated(value = EnumType.STRING)
    private MarriageStatus status;
    @ManyToMany
    private List<Person> people;

    @PreUpdate
    private void preUpdate() {
        updatedAt = new Date();
    }

    public void addMember(Person member) {
        people.add(member);
    }
}
