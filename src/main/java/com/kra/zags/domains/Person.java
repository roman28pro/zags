package com.kra.zags.domains;

import com.kra.zags.domains.enums.PersonStatus;
import com.kra.zags.domains.enums.Sex;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
@ToString(exclude = {"children", "marriages"})
@Entity
@Table(name = "people")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "second_name")
    private String secondName;
    @Column(name = "last_name")
    private String lastName;
    @Enumerated(value = EnumType.STRING)
    @Column(name = "sex")
    private Sex sex;
    @Enumerated(value = EnumType.STRING)
    private PersonStatus status;
    @Temporal(value = TemporalType.DATE)
    @Column(name = "birth_date")
    private Date birthDate;
    @Temporal(value = TemporalType.DATE)
    @Column(name = "death_date")
    private Date deathDate;
    @Column(name = "death_reason")
    private String deathReason;
    @ManyToMany
    private List<Child> children;
    @ManyToMany
    private List<Marriage> marriages;

    public void addChild(Child child) {
        children.add(child);
    }
}
