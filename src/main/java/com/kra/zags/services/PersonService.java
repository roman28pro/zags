package com.kra.zags.services;

import com.kra.zags.domains.Child;
import com.kra.zags.domains.Person;
import com.kra.zags.domains.enums.PersonStatus;
import com.kra.zags.domains.enums.Sex;
import com.kra.zags.repositories.ChildRepository;
import com.kra.zags.repositories.PersonRepository;
import com.kra.zags.services.exceptions.EntityAlreadyExistsException;
import com.kra.zags.services.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PersonService {
    private final PersonRepository personRepository;
    private final ChildRepository childRepository;

    @Autowired
    public PersonService(PersonRepository personRepository, ChildRepository childRepository) {
        this.personRepository = personRepository;
        this.childRepository = childRepository;
    }

    @Transactional
    public void addPerson(Person person, Long dadId, Long momId) throws Exception {
        person.setId(null);

        person.setStatus(PersonStatus.ALIVE);

        List<Person> parents = new ArrayList<>();

        if (dadId != null) {
            Person dad = personRepository.findById(dadId).
                    orElseThrow(() -> new Exception("Отец не найден"));

            if (dad.getSex() != Sex.MALE) {
                throw new Exception("Неверно указан отец(пол)");
            }

            if (dad.getBirthDate().after(person.getDeathDate())) {
                throw new Exception("Отец не должен быть моложе ребёнка");
            }

            parents.add(dad);
        }

        if (momId != null) {
            Person mom = personRepository.findById(momId).
                    orElseThrow(() -> new Exception("Мать не найдна"));

            if (mom.getSex() != Sex.FEMALE) {
                throw new Exception("Неверно указана мать(пол)");
            }

            if (mom.getBirthDate().after(person.getDeathDate())) {
                throw new Exception("Мать не должна быть моложе ребёнка");
            }

            parents.add(mom);
        }

        if (parents.size() != 0) {
            Child child = new Child();
            child.setChild(person);
            child.setParents(parents);

            childRepository.save(child);
        }

        personRepository.save(person);
    }

    @Transactional
    public void registerDeath(Long id, Date deathDate, String reason) throws EntityNotFoundException, EntityAlreadyExistsException {
        Person person = personRepository.findById(id).
                orElseThrow(() -> new EntityNotFoundException("Гражданин не найден"));

        if (person.getStatus() == PersonStatus.DEAD) {
            throw new EntityAlreadyExistsException("Гражданин уже зарегистрирован");
        }

        person.setStatus(PersonStatus.DEAD);
        person.setDeathDate(deathDate);
        person.setDeathReason(reason);

        personRepository.save(person);
    }

    @Transactional
    public Iterable<Person> getPersons() {
        return personRepository.findAll();
    }
}
