package com.kra.zags.services;

import com.kra.zags.domains.Marriage;
import com.kra.zags.domains.Person;
import com.kra.zags.domains.enums.MarriageStatus;
import com.kra.zags.domains.enums.PersonStatus;
import com.kra.zags.repositories.MarriageRepository;
import com.kra.zags.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MarriageService {
    private final MarriageRepository marriageRepository;
    private final PersonRepository personRepository;

    @Autowired
    public MarriageService(MarriageRepository marriageRepository, PersonRepository personRepository) {
        this.marriageRepository = marriageRepository;
        this.personRepository = personRepository;
    }

    @Transactional
    public void addMarriage(String description, Date registrationDate, Long member1Id, Long member2Id) throws Exception {
        if (description == null || description.length() == 0 || registrationDate == null ||
                member1Id == null || member2Id == null || member1Id.equals(member2Id)) {
            throw new Exception("Некорректный ввод");
        }

        Marriage marriage = new Marriage();
        marriage.setId(null);
        marriage.setDescription(description);
        marriage.setStatus(MarriageStatus.IN_MARRIAGE);
        marriage.setRegistrationDate(registrationDate);
        marriage.setPeople(new ArrayList<>());

        Person member1 = personRepository.findById(member1Id).
                orElseThrow(() -> new Exception("Первый гражданин не найден"));

        Person member2 = personRepository.findById(member2Id).
                orElseThrow(() -> new Exception("Второй гражданин не найден"));

        if (member1.getStatus() == PersonStatus.DEAD) {
            throw new Exception("Первый гражданин умер");
        }

        if (member2.getStatus() == PersonStatus.DEAD) {
            throw new Exception("Второй гражданин умер");
        }

        if (member1.getBirthDate().after(marriage.getRegistrationDate()) ||
                member2.getBirthDate().after(marriage.getRegistrationDate())) {
            throw new Exception("Гражданин не должен быть рождён позже регистрации брака");
        }

        if (member1.getSex() == member2.getSex()) {
            throw new Exception("Однополый брак");
        }

        System.out.println(member1.getMarriages());

        if (member1.getMarriages().stream().
                anyMatch(m -> m.getStatus() == MarriageStatus.IN_MARRIAGE)) {
            throw new Exception("Первый гражданин уже состоит в браке");
        }

        if (member2.getMarriages().stream().
                anyMatch(m -> m.getStatus() == MarriageStatus.IN_MARRIAGE)) {
            throw new Exception("Второй гражданин уже состоит в браке");
        }

        marriage.addMember(member1);
        marriage.addMember(member2);

        marriageRepository.save(marriage);
        member1.getMarriages().add(marriage);
        member2.getMarriages().add(marriage);
        personRepository.save(member1);
        personRepository.save(member2);
    }

    @Transactional
    public void registerDivorce(Long id, String reason, Date divorceDate) throws Exception {
        if (id == null || reason == null ||
                reason.length() == 0 || divorceDate == null) {
            throw new Exception("Некорректный ввод");
        }

        Marriage marriage = marriageRepository.findById(id).
                orElseThrow(() -> new Exception("Брак не найден"));

        if (marriage.getStatus() == MarriageStatus.IN_DIVORCE) {
            throw new Exception("Развод уже зарегистрирован");
        }

        if (marriage.getRegistrationDate().after(divorceDate)) {
            throw new Exception("Развод не должен быть зарегистрирован раньше брака");
        }

        marriage.setStatus(MarriageStatus.IN_DIVORCE);
        marriage.setDivorceReason(reason);
        marriage.setDivorceDate(divorceDate);

        marriageRepository.save(marriage);
    }

    @Transactional
    public List<Marriage> getMarriages() {
        List<Marriage> marriages = marriageRepository.findAll();

        for (Marriage marriage : marriages) {
            marriage.getPeople().size();
        }

        return marriages;
    }
}
