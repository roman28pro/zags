package com.kra.zags.services;

import com.kra.zags.domains.Child;
import com.kra.zags.repositories.ChildRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ChildService {
    private final ChildRepository childRepository;

    @Autowired
    public ChildService(ChildRepository childRepository) {
        this.childRepository = childRepository;
    }

    @Transactional
    public List<Child> getChildren() {
        List<Child> children = childRepository.findAll();
        children.forEach(child -> child.getParents().size());
        return children;
    }
}
